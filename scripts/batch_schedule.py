#!/usr/bin/env python3

from subprocess import Popen, PIPE
import sys

if len(sys.argv) < 2:
    print("Requires the amount of total processes to start as command line argument. ")
    sys.exit()
if len(sys.argv) == 2:
    n_processes_to_start = int(sys.argv[1])
    n_total_processes = n_processes_to_start
if len(sys.argv) > 2:
    n_processes_to_start = int(sys.argv[1])
    n_total_processes = int(sys.argv[2])
cmds = [["matlab", "-nosplash", "-nodesktop", f"-r 'cd ..; eg2_geodesics({i+1}, {n_total_processes}); exit;'"] for i in range(n_processes_to_start)]
proc_list = [Popen(cmd) for cmd in cmds]
for proc in proc_list:
    proc.wait()
print("Finished!")
