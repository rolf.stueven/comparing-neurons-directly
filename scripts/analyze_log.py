#!/usr/bin/env python3

import os, re, pickle
import numpy as np
from pandas import DataFrame

pattern_indices = re.compile(r"Comparing tree (\d+) with (\d+)")
pattern_results = re.compile(r"(\[\?1l>)*Distance Computation - done, total distance:(\d+\.\d+), timecost:(\d+\.\d+) secs")

def load_dataset(path):
    dataset = os.listdir(path)
    dataset = list(map(int, filter(lambda x: x, (map(lambda x: x.split(".")[0], dataset)))))
    dataset.sort()
    return dataset

def main():
    dataset_dir = input("Enter the directory of the dataset the logs are from: ") or "../datasets/IARPA/swc/subset"
    output_file = input("Enter the name of the output file: ") or "results_from_nohup.pkl"
    log_file = input("Enter the log file: ") or "logs/nohup.out"

    dataset = load_dataset(dataset_dir)
    comparisons = {"pt_root_id_1": [], "pt_root_id_2": [], "distance": [], "timecost": []}
    print(f"Dataset Size: {len(dataset)}")

    unknown_comparisons = 0
    with open(log_file, "r") as log:
        prev_index_matched = True
        indices = None
        for line in log:
            match = pattern_indices.search(line)
            if match and indices:
                prev_index_matched = False
            indices = match or indices
            results = pattern_results.search(line)
            if results:
                distance = float(results.group(2))
                time_cost = float(results.group(3))
                # If some previous indices were not matched, we cannot be sure whether the indices in the line above actually belong to the results of this distance computation:
                # Multiple simultanous running programs might have put out their logs to the file
                if indices and prev_index_matched:
                    idx1 = int(indices.group(1)) - 1
                    idx2 = int(indices.group(2)) - 1
                    pt_id_1 = dataset[idx1]
                    pt_id_2 = dataset[idx2]
                else:
                    unknown_comparisons += 1
                    pt_id_1 = -1
                    pt_id_2 = -1
                prev_index_matched = True
                indices = None
                comparisons["pt_root_id_1"].append(pt_id_1)
                comparisons["pt_root_id_2"].append(pt_id_2)
                comparisons["distance"].append(distance)
                comparisons["timecost"].append(time_cost)

    results = DataFrame(data=comparisons)
    pickle.dump(results, open(output_file, "wb"))
    print(f"Analyzed logs: {len(results)}. Results saved to {output_file}. ")
    print(f"Amount of comparisons that could not be matched to ids: {unknown_comparisons}")

if __name__ == "__main__":
    main()
