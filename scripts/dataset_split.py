#!/usr/bin/env python3

import pandas as pd
import os, time, pickle
from numpy.random import choice
from shutil import copyfile, rmtree

min_neurons_per_type = 10
ind_id = "pt_root_id"
ind_type = "cell_type_manual"

def main():
    swc_dir = input("Enter dataset directory: ") or "../datasets/IARPA/swc/"
    dest_dir = swc_dir + "subsetEvenSplit/"
    temp_file = input("The subset on which to depend as much as possible of the new dataset on") or "subset.pkl"
    csv_file = input("Enter the CSV file containing types: ") or "cell_types.csv"
    pkl_file = input("Enter the pkl file containing types: ") or "column_mapping_table_v7.pkl"

    df = pd.read_csv(csv_file)
    df = df[[ind_id, ind_type]]
    types = pd.unique(df[ind_type])

    df2 = pickle.load(open(pkl_file, "rb"))
    df2 = df2[[ind_id, ind_type]]

    df = pd.merge(df, df2, on=[ind_id, ind_type])
    available = set(df[ind_id])
    print(f"Total number of available neurons in the dataset: {df.shape[0]}")

    subset_by_type = {}

    with open(temp_file, 'rb') as file:
        subset_by_type = pickle.load(file)

    for neurons in subset_by_type.values():
        for neuron in neurons:
            if neuron not in available:
                neurons.remove(neuron)

    for typ in types:
        if not typ in subset_by_type:
            subset_by_type[typ] = []
        to_select = min(min_neurons_per_type, len(df[df[ind_type] == typ]))
        if to_select < min_neurons_per_type:
            print(f"Only found {to_select} neurons for type '{typ}'")
        while len(subset_by_type[typ]) < to_select:
            to_add = choice(df[df[ind_type] == typ][ind_id]).tolist()
            if f"{to_add}.swc" in os.listdir(swc_dir) and not to_add in subset_by_type[typ]:
                subset_by_type[typ].append(to_add)

    subset = []
    for list in subset_by_type.values():
        subset.extend(list)

    for neuron_id in subset:
        file = f"{neuron_id}.swc"
        copyfile(swc_dir + file, dest_dir + file)

    print(f"Copied {len(subset)} files. ")
    print(f"Number of files in target directory: {len(os.listdir(dest_dir))}. ")

if __name__ == "__main__":
    main()
