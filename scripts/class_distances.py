#!/usr/bin/env python3

import itertools
import os
import csv
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
from sklearn.metrics import ConfusionMatrixDisplay

ind_id = "pt_root_id"
ind_type = "cell_type_manual"

def get_swc_files(directory):
    # Get a list of SWC files in the directory
    swc_files = [file for file in os.listdir(directory) if file.endswith('.swc')]
    # Sort the SWC files in lexicographic order
    swc_files.sort()
    # These got dropped during calculation
    del swc_files[-1]
    del swc_files[26]
    del swc_files[25]
    return swc_files

def match_swcs_with_csv(swc_files, csv_file):
    # Read the CSV file with types
    df = pd.read_csv(csv_file)
    # Filter the DataFrame to include only uuid and primary type columns
    df = df[[ind_id, ind_type]]

    # Create a dictionary to store SWC files grouped by primary type
    swc_by_type = {}

    for idx, swc_file in enumerate(swc_files):
        uuid = int(os.path.splitext(swc_file)[0])
        # Find the corresponding row in the CSV by matching uuid
        matching_row = df[df[ind_id] == uuid]

        if not matching_row.empty:
            primary_type = matching_row[ind_type].values[0]
            if primary_type not in swc_by_type:
                swc_by_type[primary_type] = []
            swc_by_type[primary_type].append(idx)

    return swc_by_type

def calculate_total_distance(swc_by_type, distance_csv):
    # Read the CSV file with distance metrics
    distance_df = pd.read_csv(distance_csv, header=None)

    class_distances = np.ndarray((len(swc_by_type), len(swc_by_type)))

    for class_1, idx_list_1 in enumerate(swc_by_type.values()):
        for class_2, idx_list_2 in enumerate(swc_by_type.values()):
            # Extract the relevant rows and columns from the distance matrix
            subset_df = distance_df.iloc[idx_list_1, idx_list_2]
            # Calculate the total distance for the current primary type
            avg_distance = subset_df.values.sum() / (len(idx_list_1) * len(idx_list_2))
            class_distances[class_1, class_2] = avg_distance

    return class_distances

def main():
    swc_directory = input("Enter the directory containing SWC files: ") or "../datasets/IARPA/swc/subset"
    csv_types_file = input("Enter the CSV file containing types: ") or "cell_types.csv"
    csv_distance_file = input("Enter the CSV file containing distance metrics: ") or "distances.csv"

    # Step 1: Get SWC files
    swc_files = get_swc_files(swc_directory)

    # Step 2: Match SWCs with CSV
    swc_by_type = match_swcs_with_csv(swc_files, csv_types_file)

    # Step 3: Calculate total distances
    class_distances = calculate_total_distance(swc_by_type, csv_distance_file)

    # Display matrix
    disp = ConfusionMatrixDisplay(class_distances, display_labels=[f"{clazz}({len(ids)})" for clazz, ids in swc_by_type.items()])
    disp.plot(values_format=".0f", cmap="ocean")
    plt.show()

if __name__ == "__main__":
    main()
