import argparse
import numpy as np

def combine_matrices(input_files, output_file):
    matrices = []
    for file in input_files:
        matrix = np.loadtxt(file, delimiter=',', dtype=np.float64)  # Load as np.float64
        matrices.append(matrix)

    combined_matrix = np.inf * np.ones(matrices[0].shape, dtype=np.float64)  # Initialize as np.float64

    mismatch_indices = np.zeros(matrices[0].shape)

    for i, matrix in enumerate(matrices):
        combined_matrix = np.minimum(combined_matrix, matrix)
        mask = (combined_matrix > 0) & (matrix > 0)
        mismatch_indices += mask & (combined_matrix != matrix)

    # Save the combined matrix using NumPy
    np.savetxt(output_file, combined_matrix, delimiter=',', fmt='%.10f')  # Save as np.float64 with 10 decimal places
    # Save the mismatched indices
    np.savetxt("mismatched_indices.csv", mismatch_indices, delimiter=",", fmt='%i')

if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Combine matrices from multiple CSV files.')
    parser.add_argument('input_files', nargs='+', help='Input CSV files containing matrices')
    parser.add_argument('output_file', help='Output CSV file for the combined matrix')
    args = parser.parse_args()

    try:
        combine_matrices(args.input_files, args.output_file)
        print(f"Matrices combined and saved to {args.output_file}")
    except Exception as e:
        print(f"An error occurred: {str(e)}")
