#!/usr/bin/env python3

import os
from shutil import copyfile

def main():
    src_dir = input("Enter source directory: ") or "../outputedOBJs/geodOBJs_datasets/IARPA/swc/subset/distances/"
    dst_dir = input("Enter target directory: ") or "../outputedOBJs/geodOBJs_datasets/IARPA/swc/subsetEvenSplit/"
    csv_file = input("Enter csv file with the neuron ids: ") or "../cell_types.csv"
    subset_dir = input("Enter the dataset directory of the subset. Indices are induced from there: ") or "../datasets/IARPA/swc/subset/"

    neuron_ids = [int(os.path.splitext(file)[0]) for file in os.listdir(subset_dir) if file.endswith('.swc')]
    neuron_ids.sort()

    for file in os.listdir(src_dir):
        indices = file.split("\.")[0].split("to")
        idx1 = int(indices[0])
        idx2 = int(indices[1])

        dest_file = dst_dir + f"{neuron_ids[idx1-1]}to{neuron_ids[idx2-1]}"
        copyfile(src_dir + file, dest_file)


if __name__ == "__main__":
    main()
