#!/usr/bin/env python3

import os, re, pickle
import numpy as np
from pandas import DataFrame

pattern_indices = re.compile(r"Comparing tree (\d+) with (\d+)")
pattern_results = re.compile(r"Done, total distance:(\d+\.\d+), timecost:(\d+\.\d+) secs")

#pattern_cancelled = re.compile(r"slurmstepd: error: *** JOB (\d+) ON (\S+) CANCELLED AT (\S+) DUE TO TIME LIMIT ***")

def load_dataset(path):
    dataset = os.listdir(path)
    dataset = list(map(int, filter(lambda x: x, (map(lambda x: x.split(".")[0], dataset)))))
    dataset.sort()
    return dataset

def main():
    dataset_dir = input("Enter the directory of the dataset the logs are from: ") or "../datasets/IARPA/swc/subsetEvenSplit"
    output_file = input("Enter the name of the output file for distances: ") or "results_from_logs.pkl"
    log_dir_amount = input("Amount of directories containing logs: ") or 3
    log_dirs = []

    for i in range(log_dir_amount):
        log_dirs.append(input("Directory containing logs: ") or f"logs/logs_{i+1}")

    dataset = load_dataset(dataset_dir)
    comparisons = {"pt_root_id_1": [], "pt_root_id_2": [], "distance": [], "timecost": []}
    print(f"Dataset Size: {len(dataset)}")

    anomalous_logs = 0
    cancelled_calcs = 0
    for log_dir in log_dirs:
        for log_file_name in os.listdir(log_dir):
            log_path = os.path.join(log_dir, log_file_name)
            with open(log_path, "r") as log:
                indices = None
                results = None
                for line in log:
                    indices = pattern_indices.search(line) or indices
                    results = pattern_results.search(line) or results
                if results:
                    distance = float(results.group(1))
                    time_cost = float(results.group(2))
                else:
                    distance = np.inf
                    time_cost = np.inf
                    cancelled_calcs += 1
                if indices:
                    idx1 = int(indices.group(1)) - 1
                    idx2 = int(indices.group(2)) - 1
                    pt_id_1 = dataset[idx1]
                    pt_id_2 = dataset[idx2]
                    comparisons["pt_root_id_1"].append(pt_id_1)
                    comparisons["pt_root_id_2"].append(pt_id_2)
                    comparisons["distance"].append(distance)
                    comparisons["timecost"].append(time_cost)
                else:
                    anomalous_logs += 1


    df = DataFrame(data=comparisons)
    pickle.dump(df, open(output_file, "wb"))

    print(f"Analyzed logs: {len(df)}. Results saved to {output_file}. ")
    print(f"Anomalous logs: {anomalous_logs}")
    print(f"Cancelled Calculations: {cancelled_calcs}")

if __name__ == "__main__":
    main()
