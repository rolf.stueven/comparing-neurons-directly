#!/usr/bin/env sh
#SBATCH -c 32
#SBATCH -N 1
#SBATCH -t 2-0
#SBATCH -o slurm-long-%a.out
#SBATCH --array=1-2
#SBATCH --mail-type=END
#SBATCH --mail-user=rstueve@uni-goettingen.de

module load matlab/R2020b
module load python/3.9.0
python batch_schedule.py 32 64
