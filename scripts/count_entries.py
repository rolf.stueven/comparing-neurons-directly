#!/usr/bin/env python3

import numpy as np
import sys

def percentage_filled(matrix):
    return np.sum(matrix >= 0) / np.prod(matrix.shape) * 100

if __name__ == '__main__':
    file = sys.argv[1]
    matrix = np.loadtxt(file, delimiter=',')
    perc_filled = percentage_filled(matrix)
    print(f"Matrix is filled to {perc_filled}%")
