#!/usr/bin/env sh
#SBATCH -c 1
#SBATCH -N 1
#SBATCH -t 2-0
#SBATCH -o slurm-long-%a.out
#SBATCH --array=1-5
#SBATCH --mail-type=END
#SBATCH --mail-user=rstueve@uni-goettingen.de

module load matlab/R2020b
matlab -nosplash -nodesktop -r "cd ..; eg2_geodesics(${SLURM_ARRAY_TASK_ID}); exit;"
