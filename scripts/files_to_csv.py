#!/usr/bin/env python3

import os
import csv
import numpy as np

def main():
    dataset_dir = input("Enter the dataset directory: ") or "../datasets/IARPA/swc/subsetEvenSplit"
    input_directory = input("Enter the directory containing the files: ") or "../outputedOBJs/geodOBJs_datasets/IARPA/swc/subsetEvenSplit"

    if not os.path.isdir(input_directory):
        print("Input directory does not exist.")
        return

    output_file = input("Enter the name of the output CSV file for distances: ") or "distances.csv"
    runtime_file = input("Enter the name of the output CSV file for runtimes: ") or "run_times.csv"

    dataset = os.listdir(dataset_dir)
    dataset = list(map(int, filter(lambda x: x, (map(lambda x: x.split(".")[0], dataset)))))
    dataset.sort()
    dataset_size = len(dataset)
    print(f"Dataset size: {dataset_size}")

    csv_data = -np.ones((dataset_size, dataset_size))
    run_time_data = -np.ones((dataset_size, dataset_size))
    cellsToFill = np.ones((dataset_size, dataset_size))
    discarded_calculations = 0

    for filename in os.listdir(input_directory):
        file_path = os.path.join(input_directory, filename)

        if os.path.isfile(file_path):
            with open(file_path, 'r') as file:
                distance, run_time = file.read().split("\n")
                root_id_1, root_id_2 = map(int, os.path.splitext(filename)[0].split('to'))
                if root_id_1 not in dataset or root_id_2 not in dataset:
                    discarded_calculations += 1
                    continue
                idx1 = dataset.index(root_id_1)
                idx2 = dataset.index(root_id_2)

                cellsToFill[idx1, idx2] = 0
                cellsToFill[idx2, idx1] = 0
                csv_data[idx1, idx2] = distance
                csv_data[idx2, idx1] = distance
                run_time_data[idx1, idx2] = run_time
                run_time_data[idx2, idx1] = run_time

    np.savetxt(output_file, csv_data, delimiter=",")
    np.savetxt(runtime_file, run_time_data, delimiter=",")
    missing_cells = np.count_nonzero(cellsToFill)
    print(f"CSV file created successfully. Dimensions: {csv_data.shape}")
    print(f"Discarded calculations: {discarded_calculations}")
    print(f"Still missing calculations: {missing_cells}")
    if missing_cells:
        print(cellsToFill)

if __name__ == "__main__":
    main()
