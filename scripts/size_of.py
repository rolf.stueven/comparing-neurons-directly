#!/usr/bin/env python3

import sys
import pandas as pd

def main():
    matrix = pd.read_csv(sys.argv[1], header=None)
    print(matrix.shape)

if __name__ == "__main__":
    main()
