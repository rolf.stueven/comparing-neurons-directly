
# Readme
Data for [Cell-type-specific inhibitory circuitry from a connectomic census of mouse visual cortex](https://www.biorxiv.org/content/10.1101/2023.01.23.525290v1) (Biorxiv link).

In brief, this data archive includes information about the skeleton morphology and synaptic features of neurons whose cell bodies fell within a 100 micron by 100 micron column spanning all layers of mouse visual cortex. See [MICrONs-Explorer](https://www.microns-explorer.org/cortical-mm3) for a full description of the data and how it was collected.

The data here include both data tables of cell locations, neuronal features, synapse lists, and more, as well as files containing morphological descriptions of all neurons used for the analysis in the initial version of the preprint.

# Data Tables

## _Cell types_
*filename*: `cell_types.csv`

The location, cell id manual cell type, and automated cell type for each neuron in the column census.

### Columns:
 * `pt_root_id` : Root ID of the neuron at the time of analysis. This may change between analysis version (see above).
 * `cell_id` : Cell ID of the neuron, which will remain stable across analysis versions.
 * `m_type` : M-type of the neuron as defined in the preprint, based on feature-based classification.
 * `cell_type_manual` : Coarse cell type annotations based on human experts. Categories are:
    1. **23P**: Layer 2/3 pyramidal cell
    2. **4P**: Layer 4 pyramidal cell
    3. **5P-IT**: Intratelencephalic-projecting Layer 5 pyramidal cell
    4. **5P-ET**: Extratelencphalic-projecting Layer 5 pyramidal cell
    5. **5P-NP**: Near-projecting Layer 5 pyramidal cell
    6. **6P-IT**: Intratelencephalic-projecting Layer 6 pyramidal cell
    7. **6P-CT**: Corticothalamic-projecting Layer 6 pyramidal cell (often based on morphology and axon shape/orientation/myelination)
    8. **6P-U**: Uncertain category of Layer 6 pyramidal cell
    9. **WM-P**: White matter adjacent pyramidal cell, corresponding to Layer 6b.
    10. **BC**: Basket cell (generally soma-targeting inhibitory neuron)
    11. **BPC**: Bipolar (or multipolar) inhibitory cell based on dendritic morphology
    12. **MC**: Martinotti (or non-Martinotti) SST-like inhibitory cell
    13. **NGC**: Neurogliaform cell (including Layer 1 interneurons)
    14. **Unsure I**: Inhibitory neuron of unclear subclass
* `pt_position_um_x/y/z` : Location of the cell body in the volume measured in units of microns. 

## _Non-neuronal cells_
*filename*: `nonneuronal_cells.csv`

Location and subclass of all non-neuronal cells within the column census region.

### Columns:
* `pt_root_id` : Root ID of the cell at the time of analysis. This may change between analysis version (see above).
* `cell_type`: Manually identified cell subclass. Categories are: pericyte, astrocyte, microglia, oligo(dendrocyte), OPC (oligodendrocyte precusor cell), and unsure.
* `pt_position_um_x/y/z` : Location of the cell body in the volume measured in units of microns.

## _Excitatory properties_
*filename* : `excitatory_properties.csv`

Dendritic properties of excitatory neurons, as shown in Figure 3.

### Columns:
1. **pt_root_id**: Root ID of the neuron as analyzed.
2. **soma_depth**: Depth of the cell body from the pial surface, in microns (not used for clustering or UMAP).
3. **tip_len_dist_dendrite_p50**: Median length of dendritic tips, in microns.
4. **tip_tort_dendrite_p50**: Median tortuocity of dendritic tips (measured as the path length from tip to soma divided by euclidean distance between the same locations).
5. **num_syn_dendrite**: Number of input synapses on the dendrite.
6. **num_syn_soma**: Number of input synapses on the soma region.
7. **path_length_dendrite**: Total path length of dendritic branches, in microns.
8. **radial_extent_dendrite**: Radian extent of synaptic inputs, measured as the 95th percentile distance along the XZ plane from a streamline passing through the cell body. 
9. **syn_dist_distribution_dendrite_p50**: Median input synapse distance from the cell body.
10. **syn_size_distribution_soma_p50**: Median size of synapses onto the cell body (arbitrary units corresponding corresponding to segmented cleft volume, generally proporitional to surface area)
11. **syn_size_distribution_dendrite_p50**: Median size of synapses onto dendrites.
12. **syn_size_distribution_dendrite_dyn_range**: Difference between 5 and 95th percentile of dendritic synapse sizes.
13. **syn_depth_dist_p5**: 5th percentile of synapse depths (in microns), measuring the shallowness of the input distribution of the arbor.
14. **syn_depth_dist_p95**: 95th percentile of synapse depths (in microns), measuring the depth extent of the input distribution of the arbor.
15. **syn_depth_extent**: Difference between 95th and 5th percentile synapse depths, measuring the height of the input distribution of the arbor.
16. **max_density**: Median linear synapse density, in synapses per micron.
17. **radius_dist**: Median radius of dendrites.
18. **syn_count_pca0**: The loading on the first SparsePCA component of input synapse depth distributions. The next several columns are the same with other components.
19. **syn_count_pca1**: *idem*
20. **syn_count_pca2**: *idem*
21. **syn_count_pca3**: *idem*
22. **syn_count_pca4**: *idem*
23. **syn_count_pca5**: *idem*
24. **branch_svd0**: The loading on the first SVD component of net dendrite length as a function of distance from the cell body, capturing branching properties. The next two columns are the same, with other components.
25. **branch_svd1**: *idem*
26. **branch_svd2**: *idem*
27. **ego_count_pca0**: The loading on the first SparsePCA component of net synapse input as a function of vertical distance near the cell body. The next several columns are the same with other components.
28. **ego_count_pca1**: *idem*
29. **ego_count_pca2**: *idem*
30. **ego_count_pca3**: *idem*
31. **ego_count_pca4**: *idem*
32. **cell_type_manual**: Manual cell type classification, same as in cell_types.csv.
33. **has_ct_axon**: Only used for layer 6 neurons. 1 if the axon is myeinated into white matter, 0 if not myelinated, 0.5 if another manual subclass or unclear axon.
34. **umap0**: Location of the cell in the first component of the UMAP projection, as shown in Figure 3.
35. **umap1**: Location of the cell in the second component of the UMAP projection, as shown in Figure 3.
36. **m_type**: M-type classification, same as in cell_types.csv.
37. **cell_id**: Cell ID.

## _Input synapses_

*filname*: `all_input_synapses.csv`

All input synapses into all cells in the column data. Note that this file is very large, with ~4.4 million rows. Note that the presynaptic partners of most of these synapses are not among column cells. Only inputs onto dendrites or soma are shown, to avoid false positive detections, and any autapses are filtered out.

### Columns:
1. **synapse_id**: Unique ID of the synapse.
2. **post_pt_root_id**: Root ID of the postsynaptic neuron
3. **cell_id**: Cell ID of the postsynaptic neuron
4. **ctr_pt_position_um_x**: Location of the synapse cleft in microns (x-coordinate).
5. **ctr_pt_position_um_y**: Location of the synapse cleft in microns (y-coordinate).
6. **ctr_pt_position_um_z**: Location of the synapse cleft in microns (z-coordinate).
7. **syn_size**: Size of the synapse, measured as volume of the segmented cleft region. This is highly proportional to surface area.
8. **dist_to_root**: Geodesic distance from synapse to cell body along the dendrite.
9. **m_type**: M-type of the postsynaptic neuron.
10. **compartment**: Dendritic/somatic compartment on which the synapse was located. Categories are:
    - *soma* : Cell body or very nearby (within 15 microns from the nucleus centroid).
    - *apical* : Apical dendrite, but not proximal.
    - *prox* : Proximal dendrite.
    - *basal* : Basal dendrite, but not proximal.

## _Inhibitory synapses onto column_
*filename*: `all_input_synapses.csv`

Synapses from an inhibitory neuron in the column onto another cell in the column (of any subclass). Only axonal outputs onto dendrites or soma are shown, to avoid false positive detections and any autapses are filtered out.

### Columns:
1. **synapse_id**: Unique ID of the synapse.
2. **pre_soma_id**: Cell ID of the presynaptic neuron.
3. **post_soma_id**: Cell ID of the postsynaptic neuron.
4. **pre_pt_root_id**: Root ID of the presynaptic neuron.
5. **post_pt_root_id**: Root ID of the postsynaptic neuron.
6. **ctr_pt_position_um_x**: Location of the synapse cleft in microns (x-coordinate).
7. **ctr_pt_position_um_y**: Location of the synapse cleft in microns (y-coordinate).
8. **ctr_pt_position_um_z**: Location of the synapse cleft in microns (z-coordinate).
9. **ctr_pt_position_x**: Location of the synapse cleft in *voxels coordinates* (x-coordinate).
10. **ctr_pt_position_y**: Location of the synapse cleft in *voxels coordinates* (y-coordinate).
11. **ctr_pt_position_z**: Location of the synapse cleft in *voxels coordinates* (z-coordinate).
12. **pre_pt_position_x**: Location within the presynaptic neurite in *voxels coordinates* (x-coordinate).
13. **pre_pt_position_y**: Location within the presynaptic neurite in *voxels coordinates* (y-coordinate).
14. **pre_pt_position_z**:  Location within the presynaptic neurite in *voxels coordinates* (z-coordinate).
15. **post_pt_position_x**: Location within the postsynaptic neurite in *voxels coordinates* (x-coordinate).
16. **post_pt_position_y**:Location within the postsynaptic neurite in *voxels coordinates* (y-coordinate).
17. **post_pt_position_z**:Location within the postsynaptic neurite in *voxels coordinates* (z-coordinate).
18. **synapse_size**: Size of the synapse, measured as volume of the segmented cleft region. This is highly proportional to surface area.
19. **dist_to_root**: Geodesic distance from synapse to postsynaptic cell body along the dendrite.
20. **is_apical**: True if onto an apical compartment.
21. **is_soma**: True if on a somatic compartment.
22. **is_dendrite**: True if on any dendritic comparment (i.e. not including soma).
23. **is_proximal**: True if onto a proximal dendrite.
24. **syn_depth_um**: Depth of synapse from the pial surface, in microns.
25. **soma_depth_um**: Depth of postsynaptic soma from the pial surface, in microns.
26. **valence**: "Inh" if the postsynaptic cell is inhibitory, "Exc" otherwise.
27. **syn_in_conn**: Total number of synapses in the same reconstructed connection (i.e. from the same presynaptic neuron to the same postsynaptic neuron).
28. **is_multisyn**: True if the connection has more than one synapse.
29. **syn_clump_comp**: Synapses are considered "clumped" if they are part of the same connection and within 15 microns of geodesic distance along the presynaptic axon. The number here is a unique value identifying the synapse "clump" group on the presynaptic neuron.
30. **syn_in_clump**: Total number of synapses in the same clump.
31. **is_clumped**: True if the synapse is in a multisynaptic clump.
32. **clump_in_conn**: Number of distinct clumps in the same connection.

# Skeletons

The morphology of each neuron is provided in two skeleton formats, the standard [SWC format](http://www.neuronland.org/NLMorphologyConverter/MorphologyFormats/SWC/Spec.html) and a "Meshwork" format built from an HDF5 file that contains skeleton morphology, synapse annotations, and a few other properties. Meshwork files can be opened with the `meshwork` module of [Meshparty][https://github.com/sdorkenw/MeshParty]. Documentation about loading and using these files can be found in [this tutorial](https://github.com/AllenInstitute/swdb_2022/blob/main/DynamicBrain/EM_Connectome_Intro.ipynb).

Excitatory neurons have only dendritic morphologies, while inhibitory neurons have both axonal and dendritic regions shown. Note that axonal reconstructions are not necessarily complete, although they are generally extensive when the data made it feasible.

Skeletonization was based on a semi-coarse chunking of the neuron into 2x2x20 micron blocks, with a typical distance between vertices of slightly more than 2 microns. Distances along the skeleton less than ~5 microns should be treated as imprecise compared to longer distances.

## SWC cells

SWC cells were linearly resampled to 1 micron between vertices (except between the soma centroid and first child vertices) and given filenames `{root_id}.swc` in the directory `skeletons/swc`. Radius values were estimated based on the volume and length of segments, spans between branch points and adjacent branch points or end points. They should be considered as an average for a stretch of neurite rather than as a precise local value.

## Meshwork cells

Meshwork cells are found in `skeletons/meshwork` with filenames `{root_id}.h5`.

Meshwork files contain three aspects, `.mesh` which is a coarse spatial graph of the neuron, `.skeleton` which is a tree-like skeletonized representation based on the spatial graph, and `.anno` which contains dataframes of annotations. Every part of the neuron, including every synapse, is associated with a vertex in the spatial graph, and each skeleton vertex can represent one or more vertices in the spatial graph. Skeletons are rooted at the cell body, and all vertices except the root have a single well defined parent. Note that annotation dataframes are accessed similar to a dictionary (e.g. `nrn.anno['post_syn'].df`) or as an object (`nrn.anno.post_syn.df`).

Annotations are dataframes that include both information and a `mesh_ind` column that identifies the vertex the values are associated with (`mesh_ind_filt` is a dynamically generated index in order to track masking of the neuron object; see Meshwork tutorial above). Meshwork files here have the following tables: 
* `post_syn`: All dendritic synaptic inputs. Column definitions follow those with the same name in `all_input_synapses.csv` above.
* `pre_syn` : All axonal synaptic outputs (only populated for inhibitory neurons). Note that this includes synapses onto cells outside of the column targets.
* `vol_prop`: Volume properties of each spatial graph vertex, `area_nm2` (surface area of the chunk associated with the vertex), `size_nm3` (volume of the chunk associated with the vertex).
* `segment_properties`: For each mesh vertex, a dataframe with properties measured at the segment level, with uniform values for each segment — a span of a skeleton between adjacent branch points or between branch and end points. Columns are `area` (total surface area in nm^2), `vol` (total volume in nm^3), `area_factor`, `len` (length of the segment in nm), r_eff (approximate segment radius based on a equivalent cylinder of same length and volume), `area_factor` (measured surface area divided by the surface area of the equivalent cylinder), `strahler` (Strahler index of the segment, a measure of branch degree) and `seg_num` (a unique value per segment, letting you identify vertices along the same segment).
* `is_dendrite`, `is_axon`, `is_apical`: Each of these contains a list of spatial graph vertices that are dendritic, axonal, or apical compartments respectively. Note that apical is never used for inhibitory neurons, and axonal is not used here for exictatory neurons.

# Other Information

## Cell ID vs Root ID

A neuronal reconstruction changes due to proofreading, but the existance of a given cell does not.
Because of this, we distinguish between a unique "Cell ID" assigned to each nucleus that remains the same across proofreading and a "Root ID" that described the exact state of proofreading of a cell and change with every edit.
Root IDs are needed to visualize data and analyze any given snapshot of data, for example using Neuroglancer, while Cell IDs remain stable and can be used to track the same neurons across time points.

## Locations in space

For all tables and SWC files, coordinates are given in microns in the same coordinate system as the EM imagery available on MICrONs Explorer. Meshwork files are in nanometers, with table positions (where used) in voxels at 4x4x40 voxel resolution in order to be consistent with current CAVE analysis tools. However, when browsing in Neuroglancer, locations are specified in voxels — for this dataset, the typical voxel resolution is 4x4x40 nm/voxel. Converting a position to Neuroglancer space thus requires multiplying by 1000 (to convert to nanometers) and then dividing the x,y,z components by 4,4,40 respectively. To compare to the pial surface, look at the [Standard Transform](https://pypi.org/project/standard-transform/) package, which has a function `minnie_transform_nm` that takes points in space (in nanometers) and converts to microns with the pial surface translated to y=0.

## Known issues and important caveats

1. A small number of skeletons with very large cell bodies are poorly represented near the cell body. This will be fixed by the final version of the paper.
2. Apical labels for excitatory neurons are the result of a classifier and have not been validated on a per-cell basis.
3. Skeleton radius values were estimated on a per segment basis, where a segment refers to the span between neurite branch points or between a branch point and a tip. We estimated the radius of a segment as the radius of a cylinder with the same length and volume as measured. This gives a good rough estimate, but can be give poor values for short segments. In addiiton, it does not characterize the precise shape of neurites at short distances.  
4. Full 3d meshes will be available with the full dataset release described below.
5. The segmentation stops in the middle of layer 1, approximately 10-15 microns from the pial surface. For large tufted cells, layer 1 interneurons, and some layer 2/3 neurons, this can truncate their morphology in potentially important ways.

## Data snapshot and future plans
Neuronal reconstructions come from the larger subvolume of the MICrONs EM dataset with the proofreading and annotations frozen at materialization "v507" taken on September 26, 2022 at 8:10 am UTC.
Note that this data accompanying the preprint will focus only on the neurons described in the preprint itself, not the entire volume.
The most recent public release of the whole volume is v343, and a new public release is planned for following any revision-driven proofreading.

## Contact

If you have any questions about technical details, please contact Casey Schneider-Mizell at caseys@alleninstitute.org and if you have more general questions about the dataset please contact Nuno da Costa at nunod@alleninstitute.org.

