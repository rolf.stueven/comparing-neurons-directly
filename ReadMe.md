## Comparing neurons directly using statistical shape analysis

The accompanying code to the Bachelor thesis "Comparing neurons directly using statistical shape analysis" evaluating the use of the method by Wang et al. [1], originally developed for interpolating between trees, for the task of classifying neurons by shape. 

### How to use the code

- ```scripts/Compare Results.ipynb``` contains most of the analysis
- the main used dataset is located in ```datasets/IARPA/swc/subsetEvenSplit```
- results of the calculations are in the csv files in ```scripts/all_distances_*.csv```, where * refers to a run with specific hyperparamters
- Calculate a distance between two neurons & generate interpolating .obj
  - compile *DynamicProgrammingQ.c* using *mex* command in matlab: ```mex DynamicProgrammingQ.c```
  - run ```eg2_geodesic_neuron.m``` to compute the geodesic between a pair of neuronal trees. Adjust the data directory and indices in the file

### License

[![License: CC BY-NC 4.0](https://img.shields.io/badge/License-CC%20BY--NC%204.0-lightgrey.svg)](https://creativecommons.org/licenses/by-nc/4.0/)

This work is licensed under a [Creative Commons Attribution-NonCommercial 4.0 International License](http://creativecommons.org/licenses/by-nc/4.0/). For any commercial uses or derivatives, please contact the original authors (wangguan12621@gmail.com, H.Laga@murdoch.edu.au, anuj@stat.fsu.edu).

### References

[1] Guan Wang et al. “Elastic Shape Analysis of Tree-like 3D Objects using Extended SRVF Representation.” In: IEEE transactions on pattern analysis and machine intelligence PP (2021).
