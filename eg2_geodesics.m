function eg2_geodesics(startIdx, totalInstances)
arguments
   startIdx
   totalInstances = 5
end

addpath('utils_data', 'utils_draw', 'utils_funcs', 'utils_statModels');

data_path = 'datasets/IARPA/swc/subsetEvenSplit/';
files = get_filenames(data_path);
datasetSize = length(files);

% parameters
lam_m = 0.2;
lam_s = 1;
lam_p = 1;
global time;
global STOP_TIME;
STOP_TIME = 3600;

% ===== Load any previous calculations =====
distanceFile = "scripts/logs/distances_" + int2str(startIdx) + ".csv";
runTimeFile = "scripts/logs/run_times_" + int2str(startIdx) + ".csv";
if exist(distanceFile, "file")
    distances = readmatrix(distanceFile);
else
    distances = -ones(datasetSize);
end
if exist(runTimeFile, "file")
    runTimes = readmatrix(runTimeFile);
else
    runTimes = -ones(datasetSize);
end

% ===== Just some nice stats =====
finishedCalculations = 0;
totalCalculationsNeeded = ((datasetSize - 1) * datasetSize) / 2;
runTimeInvested = 0;
for i = 1:datasetSize
    for j = i:datasetSize
        if runTimes(i, j) > 0
            finishedCalculations = finishedCalculations + 1;
            runTimeInvested = runTimeInvested + runTimes(i, j);
        end
    end
end

fprintf("%.2f%%, %i/%i of needed calculations finished. \n", finishedCalculations / totalCalculationsNeeded * 100, finishedCalculations, totalCalculationsNeeded)
fprintf("CPU Hours done: %.0fh %.0fmin. \n", runTimeInvested / 3600, mod(runTimeInvested, 3600) / 60)

% ===== Main loop =====
for idx = startIdx:totalInstances:totalCalculationsNeeded
    [idx1, idx2] = triangularIndex(idx, datasetSize);
    shortenRecursionDepth = false;
    if distances(idx1, idx2) == -3
        fprintf('Reattempting comparison between tree %i and %i. \n', idx1, idx2)
        fprintf('Previous calculation took too long - even with shortened recursion depth - with %.4f seconds. \n', runTimes(idx1, idx2))
        shortenRecursionDepth = true;
        STOP_TIME = 36000;
    elseif distances(idx1, idx2) == -2
        fprintf('Reattempting comparison between tree %i and %i. \n', idx1, idx2)
        fprintf('Previous calculation took too long with %.4f seconds. \n', runTimes(idx1, idx2))
        shortenRecursionDepth = true;
        STOP_TIME = 36000;
    elseif runTimes(idx1, idx2) > 0
        fprintf('Skipping comparison between tree %i and %i. \n', idx1, idx2)
        fprintf('Already calculated distance is: %.4f, timecost:%.4f\n', distances(idx1, idx2), runTimes(idx1, idx2))
        continue
    end

    % ===== Load trees =====
    fprintf('Reading in neuron trees %i and %i ...\n', idx1, idx2)
    time = tic;
    Q1 = loadNeuroTree([data_path, files{idx1}]);
    Q2 = loadNeuroTree([data_path, files{idx2}]);
    timeCost = toc(time);
    fprintf('Done, timecost:%.4f secs\n', timeCost);

    % ===== Pad and Align trees =====
    fprintf('Comparing tree %i with %i\n', idx1, idx2)
    time = tic;
    if shortenRecursionDepth
        [G, Q1p, Q2p] = ReparamPerm_qCompTrees_rad_3layers_v2(Q1, Q2, lam_m, lam_s, lam_p);
    else
        [G, Q1p, Q2p] = ReparamPerm_qCompTrees_rad_4layers_v2(Q1, Q2, lam_m, lam_s, lam_p);
    end
    timeCost = toc(time);

    % ===== Write results =====
    if ~isnan(G.E)
        fprintf('Done, total distance:%.4f, timecost:%.4f secs\n', G.E, timeCost);
        distances(idx1, idx2) = G.E;
        distances(idx2, idx1) = G.E;
    else
        fprintf('Calculation cancelled due to exceeding the maximum of %i seconds\n', STOP_TIME)
        if shortenRecursionDepth
            distances(idx1, idx2) = -3;
            distances(idx2, idx1) = -3;
        else
            distances(idx1, idx2) = -2;
            distances(idx2, idx1) = -2;
        end
    end
    runTimes(idx1, idx2) = timeCost;
    runTimes(idx2, idx1) = timeCost;
    writematrix(distances, distanceFile);
    writematrix(runTimes, runTimeFile);
end
