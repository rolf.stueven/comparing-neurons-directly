#!/usr/bin/env python3

import os
import csv

def main():
    input_directory = input("Enter the directory containing the files: ")

    if not os.path.isdir(input_directory):
        print("Input directory does not exist.")
        return

    output_file = input("Enter the name of the output CSV file: ")

    csv_data = []

    for filename in os.listdir(input_directory):
        file_path = os.path.join(input_directory, filename)

        if os.path.isfile(file_path):
            with open(file_path, 'r') as file:
                cell_content = file.read()
                idx1, idx2 = map(int, os.path.splitext(filename)[0].split('to'))

                # Ensure that the CSV data list is large enough to hold the cell's content
                while len(csv_data) <= idx1:
                    csv_data.append([])

                while len(csv_data[idx1]) <= idx2:
                    csv_data[idx1].append('')

                csv_data[idx1][idx2] = cell_content

    with open(output_file, 'w', newline='') as csvfile:
        writer = csv.writer(csvfile)
        writer.writerows(csv_data)

    print("CSV file created successfully.")

if __name__ == "__main__":
    main()
