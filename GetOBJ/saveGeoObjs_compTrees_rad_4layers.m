function saveGeoObjs_compTrees_rad_4layers(A10, path)
% save geodesic objects as obj files

    CT = A10;
    for i =1: numel(CT)
        compTree2obj_rad_4layer(CT{i}, [path,'/',num2str(i)]);
    end
end
