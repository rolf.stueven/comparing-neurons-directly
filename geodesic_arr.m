function geodesic_single_index(idx)
% A way to call geodesic with just one index

addpath('utils_data');

data_path = 'datasets/IARPA/swc/subsetEvenSplit/';
files = get_filenames(data_path);

largest_ind_in_col = 0;
for i=1:length(files) - 1
    if idx > largest_ind_in_col && idx <= largest_ind_in_col + i
        idx1 = idx - largest_ind_in_col;
        idx2 = i + 1;
        break
    end
    largest_ind_in_col = largest_ind_in_col + i;
end

geodesic(idx1, idx2)


end
