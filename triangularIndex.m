function [idx1, idx2] = triangularIndex(idx, dataset_size)

largest_ind_in_col = 0;
for i=1:dataset_size - 1
    if idx > largest_ind_in_col && idx <= largest_ind_in_col + i
        idx1 = idx - largest_ind_in_col;
        idx2 = i + 1;
        break
    end
    largest_ind_in_col = largest_ind_in_col + i;
end
