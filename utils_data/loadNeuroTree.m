function [qTree, tree] = loadNeuroTree(path)

  %% read data and extract apical dendrite
  raw_tree = read_swcdata(path);
  tree = compTree_from_swcdata_rad(raw_tree, 4);
  qTree = CompTree_to_qCompTree_rad_4layers(tree);
