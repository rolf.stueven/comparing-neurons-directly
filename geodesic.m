function eg2_geodesic_neuron_no_render(idx1, idx2)

%% ############ DATA ############
% NeuroTrees
addpath('utils_data', 'utils_draw', 'utils_funcs');

data_path = 'datasets/IARPA/swc/subsetEvenSplit/';

files = get_filenames(data_path);
file_path_1 = [data_path, files{idx1}];
file_path_2 = [data_path, files{idx2}];

pt_root_id_1 = split(files{idx1}, '.');
pt_root_id_2 = split(files{idx2}, '.');
out_path = ['outputedOBJs/geodOBJs_', data_path, pt_root_id_1{1}, 'to', pt_root_id_2{1}];

fprintf('Comparing tree %i with %i\n', idx1, idx2)
% Don't recompute already calculated distances
if exist(out_path, "file")
    fprintf("Result file '%s' already exists. \n", out_path)
    return;
end

fprintf('Reading in neuron trees ...\n')
time = tic;

Q1 = loadNeuroTree(file_path_1);
Q2 = loadNeuroTree(file_path_2);

timeCost = toc(time);
fprintf('Done, timecost:%.4f secs\n', timeCost);

% ##### Geodesic Computation #####

% parameters
lam_m = 0.2;
lam_s = 1;
lam_p = 0.2;

addpath('utils_statModels')


% ===== Pad and Align trees =====
fprintf('Distance Computation ...\n')
time = tic;
[G,Q1p, Q2p] = ReparamPerm_qCompTrees_rad_4layers_v2(Q1, Q2, lam_m, lam_s, lam_p);
timeCost = toc(time);

% ===== Notify User =====
file = fopen(out_path, "w");
fprintf(file, "%d\n%f", G.E, timeCost);
fclose(file);
fprintf('Done, total distance:%.4f, timecost:%.4f secs\n', G.E, timeCost);
